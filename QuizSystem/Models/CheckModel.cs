﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuizSystem.Models
{
    public class CheckModel
    {
        public int QuizPassTry { get; set; }
        public Dictionary<int, int[]> UserAnswers { get; set; }
    }
}