﻿using BLL.Dto;
using BLL.Services;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using System;
using System.Linq;
using System.Security.Claims;
using System.Web.Mvc;

[assembly: OwinStartup(typeof(QuizSystem.App_Start.Startup))]

namespace QuizSystem.App_Start
{
    public class Startup
    {
        private IUserService CreateUserService()
        {
            return DependencyResolver.Current.GetService<IUserService>();
        }

        public void Configuration(IAppBuilder app)
        {
            app.CreatePerOwinContext<IUserService>(CreateUserService);
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                Provider = new CookieAuthenticationProvider
                {
                    OnValidateIdentity = async context =>
                    {
                        var userName = context.Identity.Name;
                        if (!String.IsNullOrEmpty(userName))
                        {
                            var service = DependencyResolver.Current.GetService<IUserService>();
                            var securityStamp = await service.GetUserSecurityStamp(userName);
                            var oldSecurityStamp = (context.Identity as ClaimsIdentity)
                                .Claims.Where(x => x.Type.Equals("AspNet.Identity.SecurityStamp"))
                                .FirstOrDefault().Value;
                            if (!securityStamp.Equals(oldSecurityStamp))
                            {
                                context.RejectIdentity();
                                context.OwinContext.Authentication.SignOut(context.Options.AuthenticationType);
                            }
                        }
                    },
                },
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                LogoutPath = new PathString("/Account/Signout")
            });
        }
    }
}