﻿using BLL.Services;
using QuizSystem.Models;
using QuizSystem.Util.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace QuizSystem.Controllers
{
    [Authorize]
    public class PassingQuizController : Controller
    {
        private readonly IPassingQuizService _quizService;
        public PassingQuizController(IPassingQuizService quizService)
        {
            _quizService = quizService;
        }

        [HttpPost]
        [HandleJsonException]
        public async Task<ActionResult> GetQuiz(int id)
        {
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
            var quiz = await _quizService.StartQuiz(id, User.Identity.Name);

            return Json(quiz, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [HandleJsonException]
        public async Task<ActionResult> Check(CheckModel answers)
        {
            Response.AppendHeader("Access-Control-Allow-Origin", "*");

            var reslt = await _quizService.CheckAnswers(answers.QuizPassTry, answers.UserAnswers);

            return Json(reslt, JsonRequestBehavior.AllowGet);
        }
    }
}