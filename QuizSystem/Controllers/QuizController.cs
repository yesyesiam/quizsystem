﻿using BLL.Dto;
using BLL.Services;
using QuizSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace QuizSystem.Controllers
{
    [Authorize]
    public class QuizController : Controller
    {
        private readonly IQuizService _quizService;
        public QuizController(IQuizService quizService)
        {
            _quizService = quizService;
        }

        [HttpGet]
        public async Task<ActionResult> Quizzes(string q = null, int page = 1)
        {
            int pageSize = 3; // количество объектов на страницу
            var list = await _quizService.GetAsync((page - 1) * pageSize, pageSize, q);

            var totalCount = list.Count() > 0 ? list.FirstOrDefault().TotalCount : 0;
            PageInfo pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = totalCount };
            QuizzesViewModel qvm = new QuizzesViewModel { PageInfo = pageInfo, Quizzes = list };

            return View(qvm);
        }

        [HttpGet]
        [Util.Filters.IfWrongParameter(Redir = "/Quiz/Quizzes")]
        public async Task<ActionResult> QuizPage(int? id)
        {
            var quiz = await _quizService.GetByIdAsync(id.GetValueOrDefault());
            if (quiz == null)
            {
                return Redirect("/Quiz/Quizzes");
            }
            return View(quiz);
        }

        [Authorize(Roles = "SuperAdmin, Admin")]
        [HttpGet]
        public async Task<ActionResult> GetQuiz(int id)//for debug
        {
            var quiz = await _quizService.GetByIdAsync(id);

            return Json(quiz, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "SuperAdmin")]
        [HttpGet]
        public async Task<ActionResult> QuizPassHistory(string user)
        {
            var history = await _quizService.GetHistoryOfPassQuizzes(0, 30, user);

            return View(history);
        }
    }
}