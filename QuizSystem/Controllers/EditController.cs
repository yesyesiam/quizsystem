﻿using BLL.Dto;
using BLL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace QuizSystem.Controllers
{
    [Authorize(Roles = "SuperAdmin, Admin")]
    public class EditController : Controller
    {
        private readonly IQuizService _quizService;
        private readonly IQuestionService _questionService;
        public EditController(IQuizService quizService, IQuestionService questionService)
        {
            _quizService = quizService;
            _questionService = questionService;
        }

        [HttpGet]
        public async Task<ActionResult> Quiz(int? id)
        {
            if (id == null)
            {
                return View("EditQuiz");
            }
            var q = await _quizService.GetByIdAsync(id.GetValueOrDefault());

            return View("EditQuiz", q);
        }

        [HttpGet]
        public async Task<ActionResult> Question(int? id, int? quiz)
        {
            if (id == null)
            {
                if (quiz == null)
                {
                    return RedirectToAction("Quiz");
                }
                ViewBag.QuizId = quiz;
                return View("EditQuestion");
            }
            var q = await _questionService.GetByIdAsync(id.GetValueOrDefault());
            if (q == null)
            {
                return RedirectToAction("Quiz");
            }

            return View("EditQuestion", q);
        }
        [HttpGet]
        public async Task<ActionResult> Answer(int? id, int? question)
        {
            if (id == null)
            {
                if (question == null)
                {
                    return RedirectToAction("Quiz");
                }
                ViewBag.QuestionId = question;
                return View("EditAnswer");
            }
            var answer = await _questionService.GetAnswerByIdAsync(id.GetValueOrDefault());
            if (answer == null)
            {
                return RedirectToAction("Quiz");
            }

            return View("EditAnswer", answer);
        }

        [HttpPost]
        public async Task<ActionResult> UpdateQuiz(QuizDTO quizDTO)
        {
            try
            {
                await _quizService.UpdateAsync(quizDTO);

                return RedirectToAction("Quiz", new { id = quizDTO.Id });
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("EditQuiz", quizDTO);
            }
        }

        [HttpPost]
        public async Task<ActionResult> CreateQuiz(QuizDTO quizDTO)
        {
            try
            {
                await _quizService.CreateAsync(quizDTO);

                return RedirectToAction("Quiz", new { id = quizDTO.Id });
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("EditQuiz", quizDTO);
            }
        }

        [HttpPost]
        public async Task<ActionResult> DeleteQuiz(int? id)
        {
            var successful = await _quizService.DeleteAsync(id.GetValueOrDefault());
            if (successful)
                return RedirectToAction("Quiz");
            else
            {
                Response.StatusCode = 400;
                return Json("fail");
            }               
        }

        [HttpPost]
        public async Task<ActionResult> CreateQuestion(QuestionDTO questionDTO)
        {
            try
            {
                await _questionService.CreateAsync(questionDTO);

                return RedirectToAction("Question", new { id = questionDTO.Id });
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("EditQuestion", questionDTO);
            }
        }
        [HttpPost]
        public async Task<ActionResult> UpdateQuestion(QuestionDTO questionDTO)
        {
            try
            {
                await _questionService.UpdateAsync(questionDTO);

                return RedirectToAction("Question", new { id = questionDTO.Id });
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("EditQuestion", questionDTO);
            }
        }

        [HttpPost]
        public async Task<ActionResult> DeleteQuestion(int? id)
        {
            var successful = await _questionService.DeleteAsync(id.GetValueOrDefault());
            if (successful>0)
                return RedirectToAction("Quiz", new { id = successful });
            else
            {
                Response.StatusCode = 400;
                return Json("fail");
            }
        }

        [HttpPost]
        public async Task<ActionResult> CreateAnswer(AnswerDTO answerDTO)
        {
            try
            {
                await _questionService.AddAnswerAsync(answerDTO);

                return RedirectToAction("Question", new { id = answerDTO.QuestionId });
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("EditAnswer", answerDTO);
            }
        }
        [HttpPost]
        public async Task<ActionResult> UpdateAnswer(AnswerDTO answerDTO)
        {
            try
            {
                await _questionService.UpdateAnswerAsync(answerDTO);

                return RedirectToAction("Question", new { id = answerDTO.QuestionId });
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("EditAnswer", answerDTO);
            }
        }
        [HttpPost]
        public async Task<ActionResult> DeleteAnswer(int? id)
        {
            var successful = await _questionService.DeleteAnswerAsync(id.GetValueOrDefault());
            if (successful>0)
                return RedirectToAction("Question", new { id = successful });
            else
            {
                Response.StatusCode = 400;
                return Json("fail");
            }
        }
    }
}