﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QuizSystem.Util.Filters
{
    /// <summary>
    /// Redirect if Id parameter is null
    /// </summary>
    public class IfWrongParameterAttribute : ActionFilterAttribute
    {
        public string Redir { get; set; }

        public IfWrongParameterAttribute()
        {
            Redir = "/Home/Index";
        }
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (context.ActionParameters.ContainsKey("id")&&context.ActionParameters["id"] == null)
            {
                context.Result = new RedirectResult(Redir);
            }
        }
    }
}