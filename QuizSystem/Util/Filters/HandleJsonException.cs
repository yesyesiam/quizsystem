﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QuizSystem.Util.Filters
{
    public class HandleJsonException : FilterAttribute, IExceptionFilter
    {
        public void OnException(ExceptionContext exceptionContext)
        {
            if (!exceptionContext.ExceptionHandled && exceptionContext.Exception is ArgumentException)
            {
                exceptionContext.HttpContext.Response.StatusCode = (int)System.Net.HttpStatusCode.BadRequest;
                exceptionContext.Result = new JsonResult()
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new
                    {
                        Error = exceptionContext.Exception.Message
                    }
                };
                exceptionContext.ExceptionHandled = true;
            }
            else if (!exceptionContext.ExceptionHandled && exceptionContext.Exception!=null)
            {
                exceptionContext.HttpContext.Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
                exceptionContext.Result = new JsonResult()
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new
                    {
                        Error = exceptionContext.Exception.Message
                    }
                };
                exceptionContext.ExceptionHandled = true;
            }
        }
    }
}