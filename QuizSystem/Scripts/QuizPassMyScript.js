﻿$(function () {
    const URL = "/PassingQuiz/GetQuiz"
    const URL2 = "/PassingQuiz/Check"

    function GetQuiz(quizId, successCallBackk, errorCallBack, alwaysCallBack) {
        $.ajax({
            method: "POST",
            url: URL,
            data: { id: quizId }
        })
            .done(successCallBackk)
            .fail(errorCallBack)
            .always(alwaysCallBack)
    }

    function CheckQuiz(answers, successCallBackk, errorCallBack, alwaysCallBack) {
        $.ajax({
            method: "POST",
            url: URL2,
            data: answers
        })
            .done(successCallBackk)
            .fail(errorCallBack)
            .always(alwaysCallBack)
    }

    function prepareAnswerState(questions) {
        let tempState = {}

        for (let i = 0; i < questions.length; i++) {
            tempState[questions[i].Id] = []
        }

        return tempState;
    }

    const startMenu = $("#start-menu")
    const startButton = $("#start-btn")
    const answerButton = $('#btn-answer')
    const endButton = $('#btn-end')
    const question = $("#question")
    const answers = $("#answers")


    const testNav = $(".navigation")
    const currentIndicator = $("#current-question-indicator")

    startButton.bind('click', StartQuiz)
    endButton.bind('click', EndQuiz)
    testNav.bind('click', Leaf)
    answerButton.bind('click', Leaf)
    answers.bind('click', SelectAnswer)


    let QUIZ_ID = testNav.data().quizid
    let PASS_TRY = null;
    let QUESTIONS = []
    let ANSWER_STATE = prepareAnswerState(QUESTIONS)
    let CURRENT_QUESTION = 0

    function OnLoadSuccess(data) {
        console.log(data);
        QUESTIONS = data.Questions
        PASS_TRY = data.QuizPassTry
        ANSWER_STATE = prepareAnswerState(QUESTIONS)
        Start()
    }

    function StartQuiz(e) {
        let btn = $(e.target)
        btn.prop("disabled", true)
        GetQuiz(QUIZ_ID, OnLoadSuccess, function (error) {
            console.log(error)
            alert("loading error. " + error?.responseJSON?.Error)
        }, function () {
            btn.prop("disabled", false)
        })
    }

    function Start() {
        answerButton.show()
        testNav.css("visibility", "visible")
        startMenu.hide()
        ShowQuestion()
    }

    function SelectAnswer(e) {
        let cQuestion = QUESTIONS[CURRENT_QUESTION]
        if ($(e.target).data().action == "off") {
            ANSWER_STATE[cQuestion.Id] = []
            ShowQuestion()
        }
        let id = $(e.target).data().id;
        if (id) {
            if (cQuestion.IsMultiple) {
                let index = ANSWER_STATE[cQuestion.Id].indexOf(id)
                if (index == -1) {
                    ANSWER_STATE[cQuestion.Id].push(id)
                } else {
                    ANSWER_STATE[cQuestion.Id].splice(index, 1)
                }
            } else {
                ANSWER_STATE[cQuestion.Id] = [id]
            }
            console.log(id)
        }
        console.log(ANSWER_STATE)
    }

    function ShowQuestion() {
        let cQuestion = QUESTIONS[CURRENT_QUESTION]
        currentIndicator.text(`${CURRENT_QUESTION + 1} / ${QUESTIONS.length}`)
        question.text(cQuestion.Text)
        answers.empty()
        for (let i = 0; i < cQuestion.Answers.length; i++) {
            let answ = cQuestion.Answers[i];
            let isCheked = ANSWER_STATE[cQuestion.Id].indexOf(answ.Id) != -1
            let divHtml = $("<div></div>")
            let inputHtml = null
            if (cQuestion.IsMultiple) {
                inputHtml = $(`<input type="checkbox" name="answer" data-id="${answ.Id}">`)
            } else {
                inputHtml = $(`<input type="radio" name="answer" data-id="${answ.Id}">`)
            }
            inputHtml.attr('checked', isCheked)
            divHtml.append(inputHtml)
            divHtml.append(`&nbsp;${answ.Text}`)
            answers.append(divHtml)
        }
        if (!cQuestion.IsMultiple && cQuestion.Answers.length == 1)
            answers.append($("<div class='btn btn-link btn-sm' data-action='off'>Снять выбор</div>"))

        if (CURRENT_QUESTION == QUESTIONS.length - 1) {
            endButton.show()
            answerButton.hide()
        } else {
            endButton.hide()
            answerButton.show()
        }
    }


    function Leaf(e) {
        let action = $(e.target).data().action;
        if (action) {
            if (action == "next") {
                CURRENT_QUESTION = CURRENT_QUESTION < QUESTIONS.length - 1 ? CURRENT_QUESTION + 1 : CURRENT_QUESTION;
            } else if (action == "prev") {
                CURRENT_QUESTION = CURRENT_QUESTION > 0 ? CURRENT_QUESTION - 1 : CURRENT_QUESTION;
            }

            console.log(CURRENT_QUESTION)
            ShowQuestion()
        }
    }

    function EndQuiz(e) {
        let btn = $(e.target)
        btn.prop("disabled", true)
        let results = {
            QuizPassTry: PASS_TRY,
            UserAnswers: []
        }

        for (let key in ANSWER_STATE) {
            results.UserAnswers.push({
                Key: key,
                Value: ANSWER_STATE[key].length > 0 ? ANSWER_STATE[key] : [0]
            })
        }

        console.log(results)
        CheckQuiz(results, function (data) {
            $("#quiz-container").hide()

            console.log(data)
            ShowResults(data)
        }, function (er) {
            alert("error")
            console.log(er)
            btn.prop("disabled", false)
        })
    }

    function ShowResults(results) {
        let rezik = results
        let divResults = $("#results")

        let testScore = $(`<p class="font-weight-bold">Резултат за тест: ${rezik.Mark}</p>`)
        divResults.append(testScore)

        if (Array.isArray(rezik.Answers)) {
            let ulHtml = $("<ul class='list-group'></ul>")
            for (let i = 0; i < QUESTIONS.length; i++) {
                let question = QUESTIONS[i]
                let userAnswer = rezik.Answers.find(x => x.Id == question.Id)?.IsCorrect
                let answerStr = "<span class='bg-danger text-white'>Неверно</span>"
                if (userAnswer == "Right") {
                    answerStr = "<span class='bg-success text-white'>Верно</span>"
                } else if (userAnswer == "Partially") {
                    answerStr = "<span class='bg-warning text-white'>Частично верно</span>"
                }
                ulHtml.append(`<li class="list-group-item">Вопрос ${i + 1}: ${question.Text}
                <br>Ответ: ${answerStr}</li>`)
            }
            divResults.append(ulHtml)
        }
        divResults.append($("<a href='javascript:window.location.href=window.location.href'>Заново</a>"))
    }
});