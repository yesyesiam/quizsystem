﻿using QuizSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace QuizSystem.Helpers
{
    public static class PagingHelpers
    {
        public static MvcHtmlString PageLinks(this HtmlHelper html,
            PageInfo pageInfo, Func<int, string> pageUrl)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 1; i <= pageInfo.TotalPages; i++)
            {
                TagBuilder liTag = new TagBuilder("li");
                TagBuilder aTag = new TagBuilder("a");
                aTag.MergeAttribute("href", pageUrl(i));
                aTag.InnerHtml = i.ToString();
                aTag.AddCssClass("page-link");

                // если текущая страница, то выделяем ее,
                // например, добавляя класс
                liTag.AddCssClass("page-item");
                if (i == pageInfo.PageNumber)
                {
                    liTag.AddCssClass("disabled");
                }
                liTag.InnerHtml = aTag.ToString();
                result.Append(liTag.ToString());
            }
            return MvcHtmlString.Create(result.ToString());
        }
    }
}