﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class Quiz
    {
        public int Id { get; set; }
        [Required]
        [MinLength(2)]
        public string Title { get; set; }
        public string Description { get; set; }
        [Required]
        public int TimeToTest { get; set; }

        public ICollection<Question> Questions { get; set; }
        public ICollection<QuizPassHistory> QuizPassHistory { get; set; }

    }
}
