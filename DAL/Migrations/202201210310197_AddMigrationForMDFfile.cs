﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMigrationForMDFfile : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.QuizPassHistory",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StartTime = c.DateTime(nullable: false),
                        EndTime = c.DateTime(),
                        Mark = c.String(),
                        ApplicationUserId = c.String(maxLength: 128),
                        QuizId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUserId, cascadeDelete: true)
                .ForeignKey("dbo.Quizes", t => t.QuizId, cascadeDelete: true)
                .Index(t => t.ApplicationUserId)
                .Index(t => t.QuizId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.QuizPassHistory", "QuizId", "dbo.Quizes");
            DropForeignKey("dbo.QuizPassHistory", "ApplicationUserId", "dbo.AspNetUsers");
            DropIndex("dbo.QuizPassHistory", new[] { "QuizId" });
            DropIndex("dbo.QuizPassHistory", new[] { "ApplicationUserId" });
            DropTable("dbo.QuizPassHistory");
        }
    }
}
