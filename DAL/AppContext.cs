﻿using DAL.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;


namespace DAL
{
    public class AppContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Quiz> Quizes { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Answer> Answers { get; set; }
        public DbSet<QuizPassHistory> QuizPassHistory { get; set; }
        public DbSet<ClientProfile> ClientProfiles { get; set; }




        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<ApplicationUser>()
                .HasOptional(u => u.ClientProfile)
                .WithRequired(cl => cl.ApplicationUser)
                .WillCascadeOnDelete(true);
            modelBuilder.Entity<Quiz>().ToTable("Quizes");

            modelBuilder.Entity<Quiz>()
                .HasMany(h => h.QuizPassHistory)
                .WithOptional(h => h.Quiz)
                .WillCascadeOnDelete(true);
            modelBuilder.Entity<ApplicationUser>()
                .HasMany(h => h.QuizPassHistory)
                .WithOptional(h => h.ApplicationUser)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<QuizPassHistory>().ToTable("QuizPassHistory");
        }
    }
}
