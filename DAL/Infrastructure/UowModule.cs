﻿using DAL.Repositories;
using Ninject.Modules;

namespace DAL.Infrastructure
{
    public class UowModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IUnitOfWork>().To<UnitOfWork>();
        }
    }
}
