﻿using DAL.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public interface IUnitOfWork : IDisposable
    {
        ApplicationUserManager UserIdentityManager { get; }
        ApplicationRoleManager RoleIdentityManager { get; }
        IUserRepository UserRepository { get; }
        IQuizRepository QuizRepository { get; }
        IQuizPassRepository QuizPassRepository { get; }
        IQuestionRepository QuestionRepository { get; }
        Task SaveAsync();
    }
}
