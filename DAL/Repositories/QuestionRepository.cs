﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using DAL.Models;

namespace DAL.Repositories
{
    public class QuestionRepository : IQuestionRepository
    {
        private readonly AppContext _appContext;
        public QuestionRepository(AppContext appContext)
        {
            _appContext = appContext;
        }

        public async Task<Question> GetByIdAsync(int id)
        {
            return await _appContext.Questions
                .Include(x => x.Answers)
                .FirstOrDefaultAsync(y=>y.Id==id);
        }

        public async Task<Question> CreateAsync(Question entity)
        {
            entity.Quiz = await _appContext.Quizes.FindAsync(entity.QuizId);
            _appContext.Questions.Add(entity);
            return entity;
        }

        public bool Update(Question entity)
        {
            var item = _appContext.Questions.Attach(entity);
            _appContext.Entry(entity).State = EntityState.Modified;

            return item != null;
        }

        public async Task<int> DeleteAsync(int id)
        {
            var element = await _appContext.Questions.FindAsync(id);
            var result = element != null;
            if (result)
            {
                _appContext.Questions.Remove(element);
            }

            return result?element.QuizId:-1;
        }

        public async Task<Answer> GetAnswerByIdAsync(int id)
        {
            return await _appContext.Answers.FindAsync(id);
        }

        public async Task<Answer> AddAnswerAsync(Answer entity)
        {
            entity.Question = await _appContext.Questions.FindAsync(entity.QuestionId);
            _appContext.Answers.Add(entity);
            return entity;
        }

        public bool UpdateAnswer(Answer entity)
        {
            var item = _appContext.Answers.Attach(entity);
            _appContext.Entry(entity).State = EntityState.Modified;

            return item != null;
        }

        public async Task<int> DeleteAnswerAsync(int id)
        {
            var element = await _appContext.Answers.FindAsync(id);
            var result = element != null;
            if (result)
            {
                _appContext.Answers.Remove(element);
            }

            return result?element.QuestionId:-1;
        }
    }
}
