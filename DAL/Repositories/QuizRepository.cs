﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class QuizRepository : IQuizRepository
    {
        private readonly AppContext _appContext;
        public QuizRepository(AppContext appContext)
        {
            _appContext = appContext;
        }

        public IQueryable<Quiz> GetAll()
        {
            return _appContext.Quizes;
        }

        public async Task<Quiz> GetByIdAsync(int id)
        {
            return await _appContext.Quizes
                .Include(x => x.Questions.Select(y=>y.Answers))
                .FirstOrDefaultAsync(i=>i.Id==id);
        }

        public Quiz Create(Quiz entity)
        {
            _appContext.Quizes.Add(entity);
            return entity;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var element = await _appContext.Quizes.FindAsync(id);
            var result = element != null;
            if (result)
            {
                _appContext.Quizes.Remove(element);
            }

            return result;
        }

        public bool Update(Quiz entity)
        {
            var item = _appContext.Quizes.Attach(entity);
            _appContext.Entry(entity).State = EntityState.Modified;
            
            return item != null;
        }
    }
}
