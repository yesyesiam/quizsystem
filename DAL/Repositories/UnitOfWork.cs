﻿using DAL.Identity;
using DAL.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private AppContext _appContext;
        private IQuizRepository _quizRepository;
        private IQuizPassRepository _quizPassRepository;
        private IQuestionRepository _questionRepository;
        private IUserRepository _userRepository;
        private ApplicationUserManager _userIdentityManager;
        private ApplicationRoleManager _roleIdentityManager;

        public UnitOfWork(AppContext appContext)
        {
            _appContext = appContext;
        }

        public IQuizRepository QuizRepository
        {
            get
            {
                if(_quizRepository==null)
                    _quizRepository = new QuizRepository(_appContext);
                return _quizRepository;
            }
        }

        public IQuestionRepository QuestionRepository
        {
            get
            {
                if(_questionRepository==null)
                    _questionRepository = new QuestionRepository(_appContext);
                return _questionRepository;
            }
        }

        public IUserRepository UserRepository
        {
            get
            {
                if (_userRepository == null)
                    _userRepository = new UserRepository(_appContext);
                return _userRepository;
            }
        }

        public ApplicationUserManager UserIdentityManager
        {
            get
            {
                if(_userIdentityManager==null)
                    _userIdentityManager = new ApplicationUserManager(new UserStore<ApplicationUser>(_appContext));
                return _userIdentityManager;
            }
        }

        public ApplicationRoleManager RoleIdentityManager
        {
            get
            {
                if(_roleIdentityManager==null)
                    _roleIdentityManager = new ApplicationRoleManager(new RoleStore<ApplicationRole>(_appContext));
                return _roleIdentityManager;
            }
        }

        public IQuizPassRepository QuizPassRepository
        {
            get
            {
                if (_quizPassRepository == null)
                    _quizPassRepository = new QuizPassRepository(_appContext);
                return _quizPassRepository;
            }
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _appContext.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public async Task SaveAsync()
        {
            await _appContext.SaveChangesAsync();
        }
    }
}
