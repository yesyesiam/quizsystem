﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public interface IUserRepository
    {
        Task<ClientProfile> GetByIdAsync(int id);
        ClientProfile Create(ClientProfile entity);
        bool Update(ClientProfile entity);
        Task<bool> DeleteAsync(int id);
    }
}
