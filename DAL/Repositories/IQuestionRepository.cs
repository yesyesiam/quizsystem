﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public interface IQuestionRepository
    {
        Task<Question> GetByIdAsync(int id);
        Task<Question> CreateAsync(Question entity);
        bool Update(Question entity);
        Task<int> DeleteAsync(int id);
        Task<Answer> GetAnswerByIdAsync(int id);
        Task<Answer> AddAnswerAsync(Answer entity);
        bool UpdateAnswer(Answer entity);
        Task<int> DeleteAnswerAsync(int id);
    }
}
