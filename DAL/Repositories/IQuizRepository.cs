﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public interface IQuizRepository
    {
        IQueryable<Quiz> GetAll();
        Task<Quiz> GetByIdAsync(int id);
        Quiz Create(Quiz entity);
        bool Update(Quiz entity);
        Task<bool> DeleteAsync(int id);
    }
}
