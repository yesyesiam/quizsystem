﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Models;

namespace DAL.Repositories
{
    class QuizPassRepository : IQuizPassRepository
    {

        private readonly AppContext _appContext;
        public QuizPassRepository(AppContext appContext)
        {
            _appContext = appContext;
        }

        public QuizPassHistory Add(QuizPassHistory entity)
        {
            _appContext.QuizPassHistory.Add(entity);
            return entity;
        }

        public async Task<QuizPassHistory> GetByIdAsync(int id)
        {
            return await _appContext.QuizPassHistory.FindAsync(id);
        }

        public bool Update(QuizPassHistory entity)
        {
            var item = _appContext.QuizPassHistory.Attach(entity);
            _appContext.Entry(entity).State = EntityState.Modified;

            return item != null;
        }

        public async Task<IEnumerable<QuizPassHistory>> GetAsyng(int take, string userName)
        {
            var q = _appContext.QuizPassHistory
                .Include(x => x.ApplicationUser);
            if (!String.IsNullOrEmpty(userName))
            {
                q = q.Where(x => x.ApplicationUser.UserName == userName);
            }
            return await q.OrderByDescending(d => d.StartTime)
                .Take(take).AsNoTracking().ToListAsync();
        }
    }
}
