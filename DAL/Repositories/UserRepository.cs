﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Models;

namespace DAL.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly AppContext _appContext;
        public UserRepository(AppContext appContext)
        {
            _appContext = appContext;
        }

        public ClientProfile Create(ClientProfile entity)
        {
            _appContext.ClientProfiles.Add(entity);
            return entity;
        }

        public Task<bool> DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task<ClientProfile> GetByIdAsync(int id)
        {
            throw new NotImplementedException();
        }

        public bool Update(ClientProfile entity)
        {
            throw new NotImplementedException();
        }
    }
}
