﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public interface IQuizPassRepository
    {
        QuizPassHistory Add(QuizPassHistory entity);
        Task<QuizPassHistory> GetByIdAsync(int id);
        bool Update(QuizPassHistory entity);
        Task<IEnumerable<QuizPassHistory>> GetAsyng(int take = 5, string userName=null);
    }
}
