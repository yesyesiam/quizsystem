﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Dto
{
    public class QuestionDTO
    {
        public int Id { get; set; }
        public bool IsMultiple { get; set; }
        public int QuizId { get; set; }
        [Required]
        [MinLength(2)]
        public string Text { get; set; }
        public List<AnswerDTO> Answers { get; set; }
    }
}
