﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Dto
{
    public class QuizPassDTO
    {
        public int Id { get; set; }
        public int QuizPassTry { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int TimeToTest { get; set; }

        public List<QuestionDTO> Questions { get; set; }
    }
}
