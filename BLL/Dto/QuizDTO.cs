﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BLL.Dto
{
    public class QuizDTO
    {
        public int Id { get; set; }
        [Required]
        [MinLength(2)]
        public string Title { get; set; }
        public string Description { get; set; }

        public int TotalCount { get; set; }

        [Range(2, 7200)]
        public int TimeToTest { get; set; }

        public List<QuestionDTO> Questions { get; set; }
    }
}
