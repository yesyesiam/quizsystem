﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Dto
{
    public class QuizPassHistoryDTO
    {
        public int Id { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public string Mark { get; set; }

        public string ApplicationUserName { get; set; }
        public string ApplicationUserId { get; set; }

        public int? QuizId { get; set; }
    }
}
