﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Dto
{
    public class AnswerDTO
    {
        public int Id { get; set; }
        public int QuestionId { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string Text { get; set; }
        public bool IsCorrect { get; set; }
    }
}
