﻿using BLL.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public interface IQuestionService
    {
        Task<QuestionDTO> GetByIdAsync(int id);
        Task<QuestionDTO> CreateAsync(QuestionDTO entity);
        Task<bool> UpdateAsync(QuestionDTO entity);
        Task<int> DeleteAsync(int id);
        Task<AnswerDTO> GetAnswerByIdAsync(int id);
        Task<AnswerDTO> AddAnswerAsync(AnswerDTO entity);
        Task<bool> UpdateAnswerAsync(AnswerDTO entity);
        Task<int> DeleteAnswerAsync(int id);
    }
}
