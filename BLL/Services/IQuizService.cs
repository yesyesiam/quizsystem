﻿using BLL.Dto;
using BLL.Infrastructure;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public interface IQuizService
    {
        Task<IEnumerable<QuizDTO>> GetAsync(int skip, int take, string filter = null);
        Task<QuizDTO> GetByIdAsync(int id);
        Task<QuizDTO> CreateAsync(QuizDTO entity);
        Task<bool> UpdateAsync(QuizDTO entity);
        Task<bool> DeleteAsync(int id);
        Task<IEnumerable<QuizPassHistoryDTO>> GetHistoryOfPassQuizzes(int skip=0, int take=5, string filter = null);
    }
}
