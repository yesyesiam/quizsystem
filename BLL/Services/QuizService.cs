﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Dto;
using BLL.Infrastructure;
using DAL.Models;
using DAL.Repositories;

namespace BLL.Services
{
    public class QuizService : IQuizService
    {
        private readonly IUnitOfWork _db;
        private IMapper _mapper;

        public QuizService(IUnitOfWork uow)
        {
            _db = uow;
            _mapper = new MapperConfiguration(cfg => cfg.AddProfile(new MapperConfig())).CreateMapper();
        }

        public async Task<QuizDTO> CreateAsync(QuizDTO entity)
        {
            var m = _mapper.Map<QuizDTO, Quiz>(entity);
            var r = _db.QuizRepository.Create(m);
            await _db.SaveAsync();
            entity.Id = r.Id;
            return entity;
        }

        public async Task<bool> UpdateAsync(QuizDTO entity)
        {
            var m = _mapper.Map<QuizDTO, Quiz>(entity);

            var result = _db.QuizRepository.Update(m);
            await _db.SaveAsync();

            return result;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                var result = await _db.QuizRepository.DeleteAsync(id);
                await _db.SaveAsync();
                return result;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<IEnumerable<QuizDTO>> GetAsync(int skip, int take, string filter)
        {
            IQueryable<Quiz> tempQ = _db.QuizRepository.GetAll();
            if (!String.IsNullOrEmpty(filter))
            {
                tempQ = tempQ.Where(x=>x.Title.Contains(filter)||x.Description.Contains(filter));
            }

            var totalCount = tempQ.Count();
            var q = tempQ
                .OrderBy(x => x.Id)
                .Skip(skip)
                .Take(take);
            var quizzes = await Task.Run(() => q.ToList());

            var rezik = _mapper.Map<IEnumerable<Quiz>, List<QuizDTO>>(quizzes);
            if(rezik.Count>0)
                rezik[0].TotalCount = totalCount;
            return rezik;
        }

        public async Task<QuizDTO> GetByIdAsync(int id)
        {
            var t = await _db.QuizRepository.GetByIdAsync(id);

            return _mapper.Map<Quiz, QuizDTO>(t);
        }

        public async Task<IEnumerable<QuizPassHistoryDTO>> GetHistoryOfPassQuizzes(int skip, int take, string filter)
        {
            var h = await _db.QuizPassRepository.GetAsyng(take, filter);

            return _mapper.Map<IEnumerable<QuizPassHistory>, List<QuizPassHistoryDTO>>(h);
        }
    }
}
