﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Dto;
using BLL.Infrastructure;
using DAL.Models;
using DAL.Repositories;
using Microsoft.AspNet.Identity;
using System.Data.Entity;

namespace BLL.Services
{
    public class UserService : IUserService
    {
        private IUnitOfWork _db;
        private IMapper _mapper;
        public UserService(IUnitOfWork uow)
        {
            _db = uow;
            _mapper = new MapperConfiguration(cfg => cfg.AddProfile(new MapperConfig())).CreateMapper();
        }
        public async Task<OperationDetails> Create(UserDTO userDto)
        {
            var user = await _db.UserIdentityManager.FindByNameAsync(userDto.UserName);
            if (user == null)
            {
                user = new ApplicationUser { UserName = userDto.UserName };
                var result = await _db.UserIdentityManager.CreateAsync(user, userDto.Password);
                if (result.Errors.Count() > 0)
                    return new OperationDetails(false, result.Errors.FirstOrDefault(), "");
                // добавляем роль
                await _db.UserIdentityManager.AddToRoleAsync(user.Id, userDto.Role);
                // создаем профиль клиента
                var clientProfile = new ClientProfile()
                {
                    Id = user.Id,
                    Name = userDto.Name
                };
                _db.UserRepository.Create(clientProfile);
                await _db.SaveAsync();
                return new OperationDetails(true, "Регистрация успешно пройдена", ""); ;
            }
            else
            {
                return new OperationDetails(false, "Пользователь с таким логином уже существует", "UserName");
            }
        }

        public async Task<ClaimsIdentity> Authenticate(UserDTO userDto)
        {
            ClaimsIdentity claim = null;

            ApplicationUser user = await _db.UserIdentityManager.FindAsync(userDto.UserName, userDto.Password);
            if (user != null)
            {
                claim = await _db.UserIdentityManager.CreateIdentityAsync(user,
                    DefaultAuthenticationTypes.ApplicationCookie);
                string fullName = user.ClientProfile.Name;
                if (!String.IsNullOrEmpty(fullName))
                    claim.AddClaim(new Claim("FullName", user.ClientProfile.Name));
            }

            return claim;
        }

        public async Task SetInitialData(UserDTO adminDto, List<string> roles)
        {
            bool isInit = false;
            foreach (string roleName in roles)
            {
                var role = await _db.RoleIdentityManager.FindByNameAsync(roleName);
                if (role == null)
                {
                    isInit = true;
                    role = new ApplicationRole { Name = roleName };
                    await _db.RoleIdentityManager.CreateAsync(role);
                }
            }
            if (isInit)
                await Create(adminDto);
            else
                throw new InvalidOperationException("error. user and roles already exist!");
        }
        public async Task<UserDTO> GetUserByIdAsync(string id)
        {
            if (id == null)
                throw new ArgumentNullException();
            ApplicationUser user = await _db.UserIdentityManager.FindByIdAsync(id);
            UserDTO mapUser = null;
            if (user != null)
            {
                mapUser = _mapper.Map<ApplicationUser, UserDTO>(user);
                var roles = await _db.UserIdentityManager.GetRolesAsync(id);
                if (roles != null)
                {
                    mapUser.Role = String.Join(",", roles);
                }
            }
            return mapUser;
        }
        public async Task<IEnumerable<UserDTO>> GetAllUsersAsync()
        {
            var users = _db.UserIdentityManager.Users.ToList();
            var userDTOs = new List<UserDTO>();
            foreach (var user in users)
            {
                var mapUser = _mapper.Map<ApplicationUser, UserDTO>(user);
                var roles = await _db.UserIdentityManager.GetRolesAsync(user.Id);
                if (roles != null)
                {
                    mapUser.Role = String.Join(",", roles);
                }
                userDTOs.Add(mapUser);
            }
            return userDTOs;
        }

        public async Task<string> GiveRole(string userId, string roleName)
        {
            if (roleName == "SuperAdmin")
            {
                throw new ArgumentException("you cannot give or take away the superadmin role");
            }
            var role = await _db.RoleIdentityManager.FindByNameAsync(roleName);
            if (role != null)
            {
                var inRole = await _db.UserIdentityManager.IsInRoleAsync(userId, role.Name);
                if (inRole)
                {
                    await _db.UserIdentityManager.RemoveFromRoleAsync(userId, role.Name);
                    var roles = await _db.UserIdentityManager.GetRolesAsync(userId);
                    if(roles.Count==0)
                        await _db.UserIdentityManager.AddToRoleAsync(userId, "User");
                }
                else
                {
                    await _db.UserIdentityManager.AddToRoleAsync(userId, role.Name);
                }
                await _db.UserIdentityManager.UpdateSecurityStampAsync(userId);
            }
            return userId;
        }

        public async Task<string> GetUserSecurityStamp(string userName)
        {
            ApplicationUser user = await _db.UserIdentityManager.FindByNameAsync(userName);

            return user!=null?user.SecurityStamp:null;
        }

        public void Dispose()
        {
            _db.Dispose();
        }
    }
}
