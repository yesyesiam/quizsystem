﻿using BLL.Dto;
using BLL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public interface IPassingQuizService
    {
        /// <summary>
        /// starts the quiz
        /// </summary>
        /// <param name="quizId">quiz id</param>
        /// <param name="user">user</param>
        /// <returns>Quiz with questions</returns>
        Task<QuizPassDTO> StartQuiz(int quizId, string user);
        /// <summary>
        /// Check quiz pass
        /// </summary>
        /// <param name="passTry">pass try id</param>
        /// <param name="answers">answers</param>
        /// <returns>Score</returns>
        Task<QuizResultsModel> CheckAnswers(int passTry, Dictionary<int, int[]> answers);
    }
}
