﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Dto;
using BLL.Infrastructure;
using DAL.Models;
using DAL.Repositories;

namespace BLL.Services
{
    public class QuestionService : IQuestionService
    {
        private readonly IUnitOfWork _db;
        private IMapper _mapper;

        public QuestionService(IUnitOfWork uow)
        {
            _db = uow;
            _mapper = new MapperConfiguration(cfg => cfg.AddProfile(new MapperConfig())).CreateMapper();
        }

        public async Task<QuestionDTO> CreateAsync(QuestionDTO entity)
        {
            var m = _mapper.Map<QuestionDTO, Question>(entity);
            var result = await _db.QuestionRepository.CreateAsync(m);
            await _db.SaveAsync();
            entity.Id = result.Id;
            entity.QuizId = result.QuizId;
            return entity;
        }

        public async Task<int> DeleteAsync(int id)
        {
            try
            {
                var result = await _db.QuestionRepository.DeleteAsync(id);
                await _db.SaveAsync();
                return result;
            }
            catch (Exception)
            {
                return -1;
            }
        }

        public async Task<QuestionDTO> GetByIdAsync(int id)
        {
            var t = await _db.QuestionRepository.GetByIdAsync(id);

            return _mapper.Map<Question, QuestionDTO>(t);
        }

        public async Task<bool> UpdateAsync(QuestionDTO entity)
        {
            var m = _mapper.Map<QuestionDTO, Question>(entity);

            var result = _db.QuestionRepository.Update(m);
            await _db.SaveAsync();
            return result;
        }

        public async Task<AnswerDTO> GetAnswerByIdAsync(int id)
        {
            var t = await _db.QuestionRepository.GetAnswerByIdAsync(id);

            return _mapper.Map<Answer, AnswerDTO>(t);
        }

        public async Task<AnswerDTO> AddAnswerAsync(AnswerDTO entity)
        {
            var m = _mapper.Map<AnswerDTO, Answer>(entity);
            var result = await _db.QuestionRepository.AddAnswerAsync(m);
            await _db.SaveAsync();
            entity.Id = result.Id;
            entity.QuestionId = result.QuestionId;
            return entity;
        }

        public async Task<bool> UpdateAnswerAsync(AnswerDTO entity)
        {
            var m = _mapper.Map<AnswerDTO, Answer>(entity);

            var result = _db.QuestionRepository.UpdateAnswer(m);
            await _db.SaveAsync();
            return result;
        }

        public async Task<int> DeleteAnswerAsync(int id)
        {
            try
            {
                var result = await _db.QuestionRepository.DeleteAnswerAsync(id);
                await _db.SaveAsync();
                return result;
            }
            catch (Exception)
            {
                return -1;
            }
        }
    }
}
