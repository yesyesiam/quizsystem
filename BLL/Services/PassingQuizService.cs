﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Dto;
using BLL.Infrastructure;
using BLL.Infrastructure.QuestionChecker;
using DAL.Models;
using DAL.Repositories;

namespace BLL.Services
{
    ///<inheritdoc cref="IPassingQuizService"/>
    public class PassingQuizService : IPassingQuizService
    {
        private readonly IUnitOfWork _db;
        private IMapper _mapper;

        public PassingQuizService(IUnitOfWork uow)
        {
            _db = uow;
            _mapper = new MapperConfiguration(cfg => cfg.AddProfile(new MapperQuizPassConfig())).CreateMapper();
        }

        public async Task<QuizPassDTO> StartQuiz(int quizId, string user)
        {
            var quiz = await _db.QuizRepository.GetByIdAsync(quizId);
            if (quiz == null || quiz.Questions.Count == 0)
            {
                throw new ArgumentException("inncorrect quiz");
            }
            var usr = await _db.UserIdentityManager.FindByNameAsync(user);
            var mapQuiz = _mapper.Map<Quiz, QuizPassDTO>(quiz);

            var passTry = new QuizPassHistory()
            {
                StartTime = DateTime.UtcNow,
                Quiz = quiz,
                ApplicationUser = usr
            };
            _db.QuizPassRepository.Add(passTry);
            await _db.SaveAsync();

            mapQuiz.QuizPassTry = passTry.Id;

            return mapQuiz;
        }

        public async Task<QuizResultsModel> CheckAnswers(int passId, Dictionary<int, int[]> answers)
        {
            DateTime nTime = DateTime.UtcNow;//время завершения теста
            var qPass = await _db.QuizPassRepository.GetByIdAsync(passId);
            if (qPass == null || answers == null || answers.Count == 0)
            {
                throw new ArgumentException("invalid input");
            }
            else if (qPass.EndTime != null)
            {
                return ReturnInvalidResult();
            }
            else
            {
                var quiz = await _db.QuizRepository.GetByIdAsync(qPass.QuizId.GetValueOrDefault());
                var result = AnswersChecking(quiz, answers);
                qPass.Mark = result.Mark;
                qPass.EndTime = nTime;

                TimeSpan span = nTime.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc));
                TimeSpan span2 = qPass.StartTime.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc));
                var diff = (int)(span - span2).TotalSeconds;
                result.Mark += $" |Вы сделали тест за {diff} сек.";
                if (diff > quiz.TimeToTest)
                {
                    result.Mark += $" |Тест просрочен.";
                    qPass.Mark = "Просрочен";
                }
                _db.QuizPassRepository.Update(qPass);
                await _db.SaveAsync();

                return result;
            }
        }

        private QuizResultsModel ReturnInvalidResult()
        {
            return new QuizResultsModel()
            {
                Mark = "Тест просрочен."
            };
        }

        private QuizResultsModel AnswersChecking(Quiz quiz, Dictionary<int, int[]> answers)
        {
            var result = new QuizResultsModel();
            IChecker checker;
            double correctPercent = 0;
            foreach (var question in quiz.Questions)
            {
                if (question.Answers.Count(x => x.IsCorrect) > 1)
                {
                    checker = new MultipleChecker();
                }
                else
                {
                    checker = new DefaultChecker();
                }
                var t = checker.Check(question, answers);
                if (t == 100d)
                {
                    result.Answers.Add(new AnswerState(question.Id, CorrectState.Right));
                }
                else if (t == 0d)
                {
                    result.Answers.Add(new AnswerState(question.Id, CorrectState.Wrong));
                }
                else
                {
                    result.Answers.Add(new AnswerState(question.Id, CorrectState.Partially));
                }
                correctPercent += t;
            }
            correctPercent /= quiz.Questions.Count;
            result.Mark = $"{Math.Round(correctPercent, 2)}%";

            return result;
        }
    }
}
