﻿using AutoMapper;
using BLL.Dto;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Infrastructure
{
    class MapperQuizPassConfig : Profile
    {
        public MapperQuizPassConfig()
        {
            CreateMap<Quiz, QuizPassDTO>();
            CreateMap<Question, QuestionDTO>()
                .ForMember(x => x.IsMultiple,
                opt => opt.MapFrom(c => c.Answers.Count(y => y.IsCorrect) > 1 ? true:false));
            CreateMap<Answer, AnswerDTO>()
                .ForMember(x => x.IsCorrect, opt => opt.MapFrom(c => false));
        }
    }
}
