﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Models;

namespace BLL.Infrastructure.QuestionChecker
{
    class DefaultChecker : IChecker
    {
        public double Check(Question question, Dictionary<int, int[]> answers)
        {
            bool result = false;
            if (answers.ContainsKey(question.Id)&&answers[question.Id].Length==1)
            {
                if (question.Answers.Count >= 1)
                {
                    var answ = question.Answers.FirstOrDefault(x => x.IsCorrect);
                    result = answ == null&&answers[question.Id][0] == 0
                        || answers[question.Id][0]==answ?.Id;
                }
                else
                {
                    result = answers[question.Id][0] == 0;
                }
            }
            else
            {
                result = false;
            }

            return result ? 100d : 0d;
        }
    }
}
