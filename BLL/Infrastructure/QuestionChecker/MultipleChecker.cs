﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Models;

namespace BLL.Infrastructure.QuestionChecker
{
    class MultipleChecker : IChecker
    {
        public double Check(Question question, Dictionary<int, int[]> answers)
        {
            int score = 0;
            if (answers.ContainsKey(question.Id)){
                var answs = question.Answers.Where(x => x.IsCorrect).Select(y=>y.Id).ToList();
                foreach (var item in answers[question.Id])
                {
                    if (answs.Contains(item))
                    {
                        score++;
                    }
                    else
                    {
                        score--;
                    }
                }
                return Math.Min(Math.Max((score * 100) / answs.Count, 0), 100);
            }
            return 0;
        }
    }
}
