﻿using BLL.Services;
using DAL.Infrastructure;
using DAL.Models;
using DAL.Repositories;
using Ninject;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Infrastructure
{
    public class ServiceModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Load(new UowModule());
            Bind<IQuizService>().To<QuizService>();
            Bind<IPassingQuizService>().To<PassingQuizService>();
            Bind<IQuestionService>().To<QuestionService>();
            Bind<IUserService>().To<UserService>();
        }
    }
}
