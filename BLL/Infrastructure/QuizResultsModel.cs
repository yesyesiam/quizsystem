﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Infrastructure
{
    public class QuizResultsModel
    {
        public QuizResultsModel()
        {
            Answers = new List<AnswerState>();
        }

        public string Mark { get; set; }

        public List<AnswerState> Answers { get; set; }
    }

    public class AnswerState
    {
        public AnswerState(int id, CorrectState isCorrect)
        {
            Id = id;
            _isCorrect = isCorrect;
        }
        public int Id { get; set; }
        private CorrectState _isCorrect;

        public String IsCorrect
        {
            get { return _isCorrect.ToString(); }
        }

    }

    public enum CorrectState
    {
        Right,
        Wrong,
        Partially
    }
}
