﻿using AutoMapper;
using BLL.Dto;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Infrastructure
{
    class MapperConfig : Profile
    {
        public MapperConfig()
        {
            CreateMap<Quiz, QuizDTO>().ReverseMap();
            CreateMap<Question, QuestionDTO>().ReverseMap();
            CreateMap<Answer, AnswerDTO>().ReverseMap();

            CreateMap<QuizPassHistory, QuizPassHistoryDTO>()
                .ForMember(x => x.ApplicationUserName, opt => opt.MapFrom(c => c.ApplicationUser.UserName))
                .ForMember(x => x.ApplicationUserId, opt => opt.MapFrom(c => c.ApplicationUser.Id));

            CreateMap<ApplicationUser, UserDTO>()
                .ForMember(x => x.Name, opt => opt.MapFrom(c => c.ClientProfile.Name));
        }
    }
}
